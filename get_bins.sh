#!/usr/bin/env bash

set -e

if [[ "$#" -eq 0 ]]; then
	echo "Requires a cpu architecture parameter (x86_64-efi, i386-pc)"
	exit 1
fi
cpu=$1

# Get the shashums for our packages
wget https://build.mergetb.net/gab/SHASUMS.txt
# Get the grub-mkstandalone image
wget https://build.mergetb.net/gab/grub-mkstandalone

# Get the modules (grub-core)
wget https://build.mergetb.net/gab/grub-core-$cpu.tar.gz

# remove all the other cpu grub-cores from shasums.txt
grep grub-core SHASUMS.txt | grep -v $cpu | xargs -I '{}' sed -zi 's/{}\n//g' SHASUMS.txt

# check our downloaded files match their respective sha sums
sha256sum -c SHASUMS.txt

# make the binary executable
chmod 755 grub-mkstandalone

# untar grub-core
mkdir ./grub-core
tar -xzf grub-core-$cpu.tar.gz
