SHELL:=/bin/bash

all: build/gabd

containers: gab-container

build/gabd: gabd/main.go gab.pb.go | build dep
	go build -o $@ $<

gab.pb.go: gab.proto
	protoc -I . ./gab.proto --go_out=plugins=grpc:.

.PHONY: gab-container
gab-container:
	./build-container.sh

dep:
	dep ensure -v

clean:
	rm -rf build
	rm -rf grub-*
	rm -f SHASUMS.txt

spotless: clean
	rm -rf vendor

build:
	mkdir build
