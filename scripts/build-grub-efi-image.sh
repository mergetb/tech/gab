#!/usr/bin/env bash

set -e

Arch="x86_64-efi"

PWD=$(pwd)
BuildDir=/opt/gab

mkdir -p $BuildDir
cd /opt/gab

# Get the shashums for our packages
wget https://build.mergetb.net/gab/SHASUMS.txt
# Get the grub-mkstandalone image
wget https://build.mergetb.net/gab/grub-mkstandalone

# Get the modules (grub-core)
wget https://build.mergetb.net/gab/grub-core-$Arch.tar.gz

# remove all the other cpu grub-cores from shasums.txt
grep grub-core SHASUMS.txt | grep -v $Arch | xargs -I '{}' sed -zi 's/{}\n//g' SHASUMS.txt

# check our downloaded files match their respective sha sums
sha256sum -c SHASUMS.txt

# make the binary executable
chmod 755 grub-mkstandalone

# untar grub-core
mkdir ./grub-core
tar -xzf grub-core-$Arch.tar.gz

# check that grub.cfg exists
if [ ! -f /boot/grub/grub.cfg ]; then
	echo "Grub configuration file missing from /boot/grub/grub.cfg"
	exit 1
fi

# build the grub binary, grub.cfg needs to exist in /boot/grub
./grub-mkstandalone -d ./grub-core -o $BuildDir/grubx64.efi -O x86_64-efi /boot/grub/grub.cfg=/boot/grub/grub.cfg

if [ ! -f $BuildDir/grubx64.efi ]; then
	echo "Failed to create grub efi file"
	exit 1
else
	echo "Created grub efi file: " $BuildDir/grubx64.efi
fi

cd $PWD

rm -rf /opt/gab
