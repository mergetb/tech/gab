#!/bin/bash

set -e

if [[ $UID -ne 0 ]]; then
  echo "must be root"
  exit 1
fi

rvn destroy
rvn build
rvn deploy
echo "Deployed raven, waiting..."
rvn pingwait grub-builder
# mount fs
rvn configure
# build inventory file
rvn status

ansible-playbook -i .rvn/ansible-hosts build-grub.yml

rvn destroy
