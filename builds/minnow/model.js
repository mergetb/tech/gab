let name = "grubbuild-"+Math.random().toString().substr(-6)

topo = {
  name: name,
  nodes: [{
    name: 'grub-builder',
    image: 'ubuntu-1804',
    cpu: { cores: 4 },
    memory: { capacity: GB(4) },
    mounts: [
      {source: env.PWD, point: '/tmp/grub'}
    ],
  }],
  links: []
}
