
client = {
	name: 'qemu+grub',
	kernel:'./build/test.bin',
	cmdline: 'console=ttyS1',
	cpu: { cores: 2 },
	memory: { capacity: GB(4) },
	disks: [
		{
			source: "/opt/sled-ci/ubuntu.qcow2",
			dev: "sda",
			bus: "ide",
		},
	],
}

topo = {
	name: 'sled-basic',
	nodes: [client],
	switches: [],
	links: []
}
