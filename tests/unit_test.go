package tests

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/gab"
	"google.golang.org/grpc"
	"os"
	"testing"
	"time"
)

// create connection to gabd
func initGRPCClient() (*grpc.ClientConn, gab.GabClient) {
	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", "gab", 9913),
		grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect to gab server - %v", err)
	}
	return conn, gab.NewGabClient(conn)
}

func TestGRPCCommunication(t *testing.T) {

	conn, gabd := initGRPCClient()
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancel()

	// this request matches with the test image in /opt/sled-ci/
	resp, err := gabd.Request(ctx,
		&gab.RRequest{
			Kernel:    "/boot/vmlinuz-4.15.0-42-generic",
			Initramfs: "/boot/initrd.img-4.15.0-29-generic",
			Uuid:      "ef0d7100-f746-11e8-96dc-525400123456",
			Label:     "",
			Kparams:   "ro maybe-ubiquity",
		})
	if err != nil {
		t.Fatalf("%v", err)
	}
	log.Infof("")

	// in docker this is a mounted parition that we can archive
	f, err := os.Create("build/test.bin")
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer f.Close()

	f.Write(resp.Grub)

}
