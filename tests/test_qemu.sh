#!/usr/bin/env bash

# High Level: this will only work if libvirt detects
# that the host has been installed, that in turn only
# happens when grub is successful.

# build topo
echo "Building topo"
rvn build

# sleep just in case
sleep 1

# start configuring the host
echo "Deploying topo"
rvn deploy

# wait 60 seconds, if pingwait returned, we get 0
# otherwise timeout returns 124, we kill rvn and fail
echo "Waiting on OS to be configured"
timeout 60 rvn pingwait qemu+grub
status=$?
if [ $status -eq 124 ]; then
	rvn destroy
	exit 1
fi

echo "OS configured, sleeping to allow completion"
# on success lets, wait until os gets through boot
sleep 10

echo "Done"
# clean up rvn
rvn destroy
exit $status
