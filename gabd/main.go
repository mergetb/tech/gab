package main

import (
	"context"
	"flag" // command line options
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/gab"
	"google.golang.org/grpc"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

//Gab protobuf struct
type Gab struct{}

var (
	version     *bool   = flag.Bool("version", false, "print gabdc version and exit")
	verbose     *bool   = flag.Bool("verbose", false, "Enabled debugging messages")
	listenIface *string = flag.String("iface", "0.0.0.0", "Specify host for gabd's grpc daemon")
	listenPort  *int    = flag.Int("port", 9913, "Specify port for gabd's grpc daemon")
	//ArchSupport which architecture should be used to create grub
	ArchSupport *string = flag.String("arch", "x86_64-efi", "Specify arch for grub (i386-pc, x86_64-efi")
	//Version gabdd version tag
	Version = "0.0.1"
)

// Request user requests a grub binary, passing in the following
// parameters: [kernel, initramfs, [uuid|label], (kernel params)
// and get out a grub-mkstandalone x86_64-efi
// Each docker container should be spawned using a specific cpu tag
// so each service is set to a single cpu arch
func (s *Gab) Request(
	ctx context.Context, e *gab.RRequest,
) (*gab.RequestResponse, error) {
	log.Infof("Request %v", e)

	t := &GRUBCFG{
		Kernel:     e.Kernel,
		Initramfs:  e.Initramfs,
		Label:      e.Label,
		UUID:       e.Uuid,
		Parameters: e.Kparams,
	}

	tmpfi, err := createGrubConfig(t)
	if err != nil {
		log.Errorf("%v", err)
		return nil, err
	}
	defer tmpfi.Close()
	defer os.Remove(tmpfi.Name())

	log.Infof("GrubConf")

	var bin *os.File
	switch arch := *ArchSupport; arch {
	case "x86_64-efi":
		bin, err = cGBx8664efi(tmpfi)
	case "i386-pc":
		bin, err = cGBi386pc(tmpfi)
	default:
		return nil, fmt.Errorf("Gab does not understand: %s", arch)
	}
	if err != nil {
		log.Errorf("%v", err)
		return nil, err
	}
	defer bin.Close()
	defer os.Remove(bin.Name())

	log.Infof("GrubBin")

	data, err := ioutil.ReadAll(bin)
	if err != nil {
		log.Errorf("%v", err)
		return nil, err
	}

	rr := &gab.RequestResponse{
		Grub: data,
	}
	return rr, nil
}

func main() {

	flag.Parse()

	if *version {
		fmt.Printf("Gabd Version: %s", Version)
		os.Exit(0)
	}

	log.Infof("Starting gabd-server.")

	if *verbose {
		log.SetLevel(log.DebugLevel)
	}

	grpcServer := grpc.NewServer()
	gab.RegisterGabServer(grpcServer, &Gab{})

	protobufServer, err := net.Listen("tcp", fmt.Sprintf("%s:%d", *listenIface, *listenPort))
	if err != nil {
		log.Fatalf("protobuf failed to listen: %v", err)
	}
	// spin this off as a goroutine
	log.Infof("Listening on tcp://%s:%d", *listenIface, *listenPort)
	grpcServer.Serve(protobufServer)
}

func readFile(name string) error {
	log.Infof("Reading: %s", name)
	file, err := os.Open(name)
	if err != nil {
		return err
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}
	fmt.Print(string(b))
	return nil
}

func writeFile(content string) (*os.File, error) {
	tmpfile, err := ioutil.TempFile("", "grub.cfg")
	if err != nil {
		return nil, err
	}
	//defer os.Remove(tmpfile.Name()) // clean up
	if _, err := tmpfile.Write([]byte(content)); err != nil {
		return nil, err
	}
	if err := tmpfile.Close(); err != nil {
		return nil, err
	}
	return tmpfile, nil
}

//GRUBCFG values that will fill out our grub cfg
type GRUBCFG struct {
	Kernel     string
	Initramfs  string
	UUID       string
	Label      string
	Parameters string
	Search     string
	Root       string
}

var uuidString = "--fs-uuid"
var labelString = "--label"

//fedora may use initrd16, have not tested
func createGrubConfig(config *GRUBCFG) (*os.File, error) {
	// only use UUID or Label, if both provided, use UUID
	if config.UUID != "" {
		config.Search = fmt.Sprintf("%s %s", uuidString, config.UUID)
		config.Root = fmt.Sprintf("root=UUID=%s", config.UUID)
	} else {
		log.Infof("%#v %#v", labelString, config.Label)
		config.Search = fmt.Sprintf("%s %s", labelString, config.Label)
		config.Root = fmt.Sprintf("root=Label=%s", config.Label)
	}

	t, err := template.New("grub.cfg").Parse(`
default=0
timeout=0

menuentry "sled grub" --id zues {
	insmod part_gpt #used by ubuntu
	insmod part_msdos #used by fedora
	insmod ext2 # ubuntu
	insmod xfs # fedora
	insmod gzio # ubuntu, fedora
	insmod biosdisk # need for i386
	search --no-floppy --set=root {{ .Search}}
	linux {{ .Kernel}} {{ .Root}} {{ .Parameters}}
	initrd {{ .Initramfs}}
}
`)
	if err != nil {
		return nil, err
	}

	tmpfile, err := ioutil.TempFile("", "grub.cfg")
	if err != nil {
		return nil, err
	}

	err = t.Execute(tmpfile, config)
	if err != nil {
		//specifically remove the file here, upper layer only
		//handles deletion on good return
		os.Remove(tmpfile.Name())
		return nil, err
	}

	return tmpfile, err
}

//cGBx8664efi stands for create grub binary x86_64-efi. it
// takes as input the grub.cfg (temp file)
// and return to the user the mkstandalone image with the
// grub.cfg embedded
func cGBx8664efi(grubcfg *os.File) (*os.File, error) {
	tmpfi, err := ioutil.TempFile("", "grub.bin")
	if err != nil {
		log.Errorf("%v", err)
		return nil, err
	}

	config := fmt.Sprintf(
		"boot/grub/grub.cfg=%s", grubcfg.Name())

	preamble := fmt.Sprintf(
		"-d ./grub-core -O x86_64-efi -o %s", tmpfi.Name())

	cmdString := append(strings.Split(preamble, " "), config)

	cmd := exec.Command("./grub-mkstandalone", cmdString...)

	log.Infof("cmd: %v", cmd)

	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Errorf("%v: %s", err, string(out))
		//specifically remove the file here, upper layer only
		//handles deletion on good return
		os.Remove(tmpfi.Name())
		return nil, err
	}

	log.Infof("%v: %s", cmd, out)

	return tmpfi, nil
}

func cGBi386pc(grubcfg *os.File) (*os.File, error) {
	// this will run in the container, with grub-core (where all
	// the modules are located) in the cwd.
	// grub-mkstandalone is also in the cwd

	tmpfi, err := ioutil.TempFile("", "grub.bin")
	if err != nil {
		log.Errorf("%v", err)
		return nil, err
	}

	modules := "--install-modules=ahci all_video at_keyboard biosdisk cat chain " +
		"configfile cpio disk drivemap ehci elf ext2 fat font gzio gptsync " +
		"hello help iso9660 linux ls lvm msdospart multiboot2 normal " +
		"part_gpt part_msdos serial usb_keyboard vga_text xfs zfs echo " +
		"search"
	config := fmt.Sprintf(
		"boot/grub/grub.cfg=%s", grubcfg.Name())

	preamble := fmt.Sprintf(
		"-d ./grub-core -O i386-pc -o %s --locales= --themes= --fonts= ", tmpfi.Name())

	cmdString := append(strings.Split(preamble, " "), modules, config)

	cmd := exec.Command("./grub-mkstandalone", cmdString...)

	log.Infof("cmd: %v", cmd)

	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Errorf("%v: %s", err, string(out))
		//specifically remove the file here, upper layer only
		//handles deletion on good return
		os.Remove(tmpfi.Name())
		return nil, err
	}

	log.Infof("%v: %s", cmd, out)

	return tmpfi, nil
}
