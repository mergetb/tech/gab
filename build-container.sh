#!/usr/bin/env bash

#==============================================================================
# build and deploy the mergetb/gab container
#==============================================================================

set -e
set -E

ARCH=("ARCHSUPPORT=x86_64-efi" "ARCHSUPPORT=i386-pc")

for arch in ${ARCH[@]}; do
  TAG=$(echo $arch | cut -d "=" -f 2)
  echo $arch $TAG
  docker build --no-cache --build-arg $arch -f gab.dock -t quay.io/mergetb/gab:$TAG .
  if [[ ! -z "$PUSH" ]]; then
    docker push quay.io/mergetb/gab:$TAG
  fi
done
